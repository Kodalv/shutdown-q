package src.main.java;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleGUI extends JFrame {

    private JButton button = new JButton("Turn off");
    private JTextField text = new JTextField("", 5);
    private JLabel label = new JLabel(" Time in minutes:");

    public SimpleGUI() {
        super(" Power off timer");
        this.setBounds(100, 100, 320, 130);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(3, 2, 5, 5));
        container.add(label);
        container.add(text);

        button.addActionListener(new ButtonEventListener());
        container.add(button);
    }


    class ButtonEventListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            ShutDown.shut(text.getText());

            JOptionPane.showMessageDialog(null,
                    "  Your PC will shut down after " + text.getText() + " minutes!",
                    "Warning",
                    JOptionPane.PLAIN_MESSAGE);

        }
    }
}

